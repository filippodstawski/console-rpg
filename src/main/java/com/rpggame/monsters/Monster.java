package com.rpggame.monsters;

public abstract class Monster {

    private int hp;

    public abstract int getInitialHp();
    public abstract int getMinPower();
    public abstract int getMaxPower();
    public abstract int getExperience();
    public abstract String getName();
    public abstract String getDescription();

    Monster(){
        this.hp = getInitialHp();
    }

    public String getAccuratelyHitDescription(){
        return "Przeciwnik trafił Cię!";
    }

    public String getNotAccuratelyHitDescription(){
        return "Przeciwnik spudłował!";
    }

    public String getMonsterIsDead(){
        return "Przeciwnik jest martwy!";
    }

    public void substractFromHp(int hitPower){
        this.hp = this.hp - hitPower;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }
}
